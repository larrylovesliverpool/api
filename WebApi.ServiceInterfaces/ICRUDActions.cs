﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.ServiceInterfaces
{
    public interface ICRUDActions<dataObject>
    {
        IEnumerable<dataObject> Read();

        dataObject Read(int identifier);

        IStatus Create(dataObject T);

        IStatus Update(dataObject T);

        IStatus Delete(int identifier);
    }
}
