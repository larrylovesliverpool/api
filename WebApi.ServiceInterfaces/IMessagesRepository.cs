﻿

using System.Collections.Generic;
using WebApi.Domain;

namespace WebApi.ServiceInterfaces
{
    public interface IMessagesRepository : ICRUDActions<Message> { }
}
