﻿
using WebApi.Domain;

namespace WebApi.ServiceInterfaces
{
    public interface IApplicationRepository : ICRUDActions<Application> {}
}
