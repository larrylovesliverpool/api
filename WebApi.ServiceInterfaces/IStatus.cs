﻿



namespace WebApi.ServiceInterfaces
{
    public interface IStatus
    {
        bool IsValid { get; set; }
        string text { get; set; }
        void CatchMessage();
    }
}
