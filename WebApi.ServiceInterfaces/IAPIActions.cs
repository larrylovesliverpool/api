﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.ServiceInterfaces
{
    public interface IAPIActions<dataOject>
    {
        IEnumerable<dataOject> read();

        dataOject read(int identifier);
    }
}
