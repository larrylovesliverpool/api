﻿
using System.Collections.Generic;
using WebApi.ServiceInterfaces;

namespace WebApi.Orchestration
{
    public class ApplicationsMessages
    {
        IApplicationsMessagesRepository appsMessagesRepo;

        public ApplicationsMessages()
        {
            appsMessagesRepo = new Respository.ApplicationsMessagesRepository();
        }

        public ApplicationsMessages(IApplicationsMessagesRepository repo)
        {

            if (repo != null)
                appsMessagesRepo = repo;
            else
                throw new System.Exception();
        }

        public IEnumerable<Domain.ApplicationsMessages> read() => appsMessagesRepo.Read();

        public Domain.ApplicationsMessages read(int id) => appsMessagesRepo.Read(id);
    }
}
