﻿using System.Collections.Generic;
using WebApi.Domain;
using WebApi.Respository;
using WebApi.ServiceInterfaces;

namespace WebApi.Orchestration
{
    public class Applications
    {
        IApplicationRepository applicationRepo;

        public Applications()
        {
            applicationRepo = new ApplicationRepository();
        }

        public Applications(IApplicationRepository repo)
        {

            if (repo != null)
                applicationRepo = repo;
            else
                throw new System.Exception();
        }

        public IEnumerable<Application> read() => applicationRepo.Read();

        public Application read(int identifier) => applicationRepo.Read(identifier);
    }
}
