﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WebApi.ServiceInterfaces;
using WebApi.Respository;
using WebApi.Domain;

namespace WebApi.Orchestration
{
    public class Messages
    {
        IMessagesRepository messageRepo;

        public Messages()
        {
            messageRepo = new MessagesRepository();
        }

        public Messages(IMessagesRepository repo) {

            if (repo != null)
                messageRepo = repo;
            else
                throw new System.Exception();
        }

        // Read all messages
        public IEnumerable<Message> Read() => messageRepo.Read();

        // Read individual message 
        public Message Read(int id)
        {
            return messageRepo.Read().FirstOrDefault( p => p.messageID == id );
        }

        // Read active individual message
        public Message ReadMessage(int id)
        {
           return messageRepo.Read().FirstOrDefault(p => p.messageID == id && ( p.startShowing <= DateTime.Now && p.startShowing.AddDays(p.numberOfDays) >= DateTime.Now));
        }

        // Read active message for application
        public Message GetMessage(int id)
        {
            return messageRepo.Read().FirstOrDefault(p => p.selectedApplication == id && (p.startShowing <= DateTime.Now && p.startShowing.AddDays(p.numberOfDays) >= DateTime.Now));
        }

        public IStatus CreateMessage(Message message) => messageRepo.Create(message);

        public IStatus AmendMessage(Message message) => messageRepo.Update(message);
    }
}
