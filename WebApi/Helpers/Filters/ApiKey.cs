﻿using System.Configuration;
using System.Web.Mvc;

namespace WebApi.Helpers.Filters
{
    public class ApiKeyActionWebApiFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            string apiKey = ConfigurationManager.AppSettings["ApiKey"];
        }
    }
}