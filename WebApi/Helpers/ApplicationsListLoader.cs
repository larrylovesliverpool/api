﻿
//

using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApi.Helpers
{
    public static class ApplicationsListLoader
    {
        public static List<SelectListItem> Create()
        {
            var applications = new WebApi.Orchestration.Applications().read();
            var ListOf = new List<SelectListItem>();

            ListOf.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach ( var application in applications)
            {
                ListOf.Add(new SelectListItem { Text = application.name, Value = application.applicationID.ToString() });
            }

            return ListOf;
        }
    }
}