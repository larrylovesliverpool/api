﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using WebApi.Domain;

namespace WebApi.Controllers
{
    public class ApplicationsController : Controller
    {
        public ActionResult Index()
        {
            // Generate view model to display page
            var viewModel = new ViewModels.ApplicationsViewModel();
            // Generate page data
            var systems = new Orchestration.Applications().read();

            return View(viewModel.GenerateViewModel(systems));
        }

        public ActionResult Errors()
        {
            return View();
        }
    }
}
