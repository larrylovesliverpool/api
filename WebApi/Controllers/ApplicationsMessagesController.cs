﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApi.Controllers
{
    public class ApplicationsMessagesController : Controller
    {
        // GET: ApplicationsMessages
        public ActionResult Index()
        {
            // Generate view model to display page
            var viewModel = new ViewModels.ApplicationsMessagesViewModel();
            // Generate page data
            var systemMessages = new Orchestration.ApplicationsMessages().read();

            return View();
        }
    }
}