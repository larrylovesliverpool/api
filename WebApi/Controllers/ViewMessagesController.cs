﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApi.ViewModels;

namespace WebApi.Controllers
{
    public class ViewMessagesController : Controller
    {
        Orchestration.Messages actions = new Orchestration.Messages();

        // GET: ViewMessages
        public ActionResult Index()
        {
            // Generate view model to display page
            var viewModel = new ViewModels.MessagesViewModel();
            // Generate page data
            var messages = actions.Read();

            return View(viewModel.GenerateViewModel(messages));
        }

        public ActionResult Create() {

            var viewModel = new WebApi.ViewModels.MessagesCreateViewModel();

            return View(viewModel);
        }

        public ActionResult Amend(int id) {

            var viewModel = new WebApi.ViewModels.MessagesAmendViewModel(id);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Create(MessagesCreateViewModel messageVM )
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Domain.Message message = AutoMapper.Mapper.Map<Domain.Message>(messageVM);

                    if (actions.CreateMessage(message).IsValid)
                    {
                        return RedirectToAction("Index");
                    }
                }
                return View(messageVM);
            }
            catch (Exception ex)
            {
                return View(messageVM);
            }
        }

        [HttpPost]
        public ActionResult Amend(MessagesAmendViewModel messageVM )
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Domain.Message message = AutoMapper.Mapper.Map<Domain.Message>(messageVM);

                    if (actions.AmendMessage(message).IsValid)
                    {
                        return RedirectToAction("Index");
                    }
                }
                return View(messageVM);
            }
            catch(Exception ex)
            {
                return View(messageVM);
            }
        }

        public ActionResult Cancel() => RedirectToAction("Index");
    }
}