﻿using System.Web.Http;
using WebApi.Helpers.Filters;
using WebApi.Orchestration;

namespace WebApi.Controllers
{
    public class MessagesController : ApiController
    {
        Messages messages = new Messages();

        // api call read all messages
        [HttpGet]
        public IHttpActionResult ReadMessages() => Ok(messages.Read());

        // api call to read a message
        [HttpGet]
        public IHttpActionResult ReadMessages(int id) => Ok(messages.Read(id));

        // api call Get all messages
        [HttpGet]
        public IHttpActionResult GetMessages() => Ok(messages.Read());

        // api call to Get a message
        [HttpGet]
        public IHttpActionResult GetMessages(int id)
        {
            var message = messages.GetMessage(id);

            if ( message == null )
            {
                return NotFound();
            }

            return Ok(message);
        }

        [HttpGet]
        [ApiKeyActionWebApiFilter]
        public IHttpActionResult GetMessages(int id, int key)
        {
            var message = messages.GetMessage(id);

            if (message == null)
            {
                return NotFound();
            }

            return Ok(message);
        }

        [HttpDelete]
        public IHttpActionResult DeleteMessages(int id)
        {
            return Ok();
        }
    }
}
