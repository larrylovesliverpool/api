﻿using System.Web.Http;

using WebApi.Orchestration;

namespace WebApi.Controllers
{
    public class SystemsController : ApiController
    {
        Applications applications = new Applications();

        [HttpGet]
        public IHttpActionResult GetApplications() => Ok(applications.read());

        [HttpGet]
        public IHttpActionResult GetApplications(int id) => Ok(applications.read(id));
       
    }
}
