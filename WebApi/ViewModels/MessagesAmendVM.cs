﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApi.Domain;

namespace WebApi.ViewModels
{
    public class MessagesAmendViewModel : IMessage
    {
        public DateTime pageCreatedAt { get; }

        [Required]
        [Display(Name = "Message Text")]
        [StringLength(256)]
        public string text { get; set; }

        [Display(Name = "Identifier")]
        public int messageID { get; set; }

        [Required]
        [Display(Name = "Start Showing on")]
        public DateTime startShowing { get; set; }

        [Required]
        [Display(Name = "For days")]
        [Range(1, 30)]
        public int numberOfDays { get; set; }

        [Display(Name = "Application")]
        public List<SelectListItem> applicationsList { get; set; }

        public int selectedApplication { get; set; }

        public MessagesAmendViewModel()
        {
            applicationsList = WebApi.Helpers.ApplicationsListLoader.Create();
            pageCreatedAt = DateTime.Now;
        }

        public MessagesAmendViewModel(int id) {

            var message = new WebApi.Orchestration.Messages().Read(id);

            this.text = message.text;
            this.startShowing = message.startShowing;
            this.numberOfDays = message.numberOfDays;
            this.selectedApplication = message.selectedApplication;
            this.messageID = message.messageID;

            applicationsList = WebApi.Helpers.ApplicationsListLoader.Create();
            pageCreatedAt = DateTime.Now;
        }

    }
}