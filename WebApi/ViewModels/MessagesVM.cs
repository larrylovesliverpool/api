﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Domain;

namespace WebApi.ViewModels
{
    public class MessagesDisplayModel : Message
    {
        public string showDate { get; set; }
        public bool active { get; set; }
        public string tableRowDisplay { get; set; }
        public string tableActiveText { get; set; }
        public string tableActiveTextDisplay { get; set; }
        public string tableMessageColDisplay { get; set; }
        public string tableMessageColText { get; set; }
    }

    public class MessagesViewModel : IViewModel
    {
        public DateTime pageCreatedAt { get; }

        public IEnumerable<MessagesDisplayModel> view { get; set; }

        public MessagesViewModel()
        {
            this.pageCreatedAt = DateTime.Now;
        }

        public MessagesViewModel GenerateViewModel(IEnumerable<Message> messages)
        {
            // clone to view model
            this.view = ToClone(messages);
            ToDisplay();

            return this;
        }

        private IList<MessagesDisplayModel> ToClone(IEnumerable<Message> messages)
        {

            IList<MessagesDisplayModel> clone = new List<MessagesDisplayModel>();

            foreach (var message in messages)
            {
                clone.Add(new MessagesDisplayModel { messageID = message.messageID,
                                                     text = message.text,
                                                     startShowing = message.startShowing,
                                                     numberOfDays = message.numberOfDays });
            }

            return clone;
        }

        private void ToDisplay() {

            foreach( var message in this.view)
            {
                message.showDate = message.startShowing.ToShortDateString();

                if ((message.startShowing <= DateTime.Now && message.startShowing.AddDays(message.numberOfDays) >= DateTime.Now))
                {
                    message.active = true;
                    message.tableActiveText = "Active";
                    message.tableActiveTextDisplay = "info";
                }
            }
        }

    }
}