﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApi.Domain;

namespace WebApi.ViewModels
{
    class MessageCreateDisplayModel {


    }

    public class MessagesCreateViewModel : IMessage

    {
        public DateTime pageCreatedAt { get; }

        [Required]
        [Display(Name = "Message Text")]
        [StringLength(256)]
        public string text { get; set; }

        public int messageID { get; set; }

        [Required]
        [Display(Name = "Start Showing on")]
        public DateTime startShowing { get; set; }

        [Required]
        [Display(Name = "For days")]
        [Range(1, 30)]
        public int numberOfDays { get; set; }

        [Display(Name = "Application")]
        public List<SelectListItem> applicationsList { get; set; }

        [Required]
        public int selectedApplication { get; set; }

        public MessagesCreateViewModel() {

            this.text = string.Empty;
            this.startShowing = DateTime.Now;
            this.numberOfDays = 1;

            applicationsList = WebApi.Helpers.ApplicationsListLoader.Create();
        }
    }
}