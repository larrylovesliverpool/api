﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApi.Domain;

namespace WebApi.ViewModels
{
    public class ApplicationsDisplayModel : Application
    {
        public string tableRowDisplay { get; set; }
        public string tableActiveText { get; set; }
        public string tableMessageColDisplay { get; set; }
        public string tableMessageColText { get; set; }

        public ApplicationsDisplayModel() {}
    }

    public class ApplicationsViewModel
    {
        public DateTime pageCreatedAt { get; }

        public IEnumerable<ApplicationsDisplayModel> view { get; set; }

        public ApplicationsViewModel() {
            this.pageCreatedAt = DateTime.Now;
        }

        public ApplicationsViewModel GenerateViewModel(IEnumerable<Application> systems)
        {           
            ToViewModel(systems);

            return this;
        }

        private void ToViewModel(IEnumerable<Application> systems)
        {
            this.view = ToClone(systems);
            ToDisplay();
        }

        private IList<ApplicationsDisplayModel> ToClone(IEnumerable<Application> systems)
        {

            IList<ApplicationsDisplayModel> clone = new List<ApplicationsDisplayModel>();

            foreach (var system in systems)
            {
                var cloned = new ApplicationsDisplayModel
                {
                    applicationID = system.applicationID,
                    name = system.name,
                    description = system.description,
                    active = system.active
                };

                clone.Add(cloned);
            }

            return clone;
        }

        private void ToDisplay()
        {

            foreach (var item in this.view)
            {
                if (!item.active)
                {
                    item.tableActiveText = "";
                    item.tableRowDisplay = "active";
                }
                else
                {
                    item.tableActiveText = "Active";
                    item.tableRowDisplay = "info";
                }
            }

        }
    }
}