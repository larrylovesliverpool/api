﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Domain;

namespace WebApi.ViewModels
{

    public class ApplicationsMessagesDisplayModel : ApplicationsMessages
    {
        public string tableRowDisplay { get; set; }
        public string tableActiveText { get; set; }
        public string tableMessageColDisplay { get; set; }
        public string tableMessageColText { get; set; }

        public ApplicationsMessagesDisplayModel() { }
    }

    public class ApplicationsMessagesViewModel
    {
        public DateTime pageCreatedAt { get; }

        public IEnumerable<ApplicationsMessagesDisplayModel> view { get; }

        public ApplicationsMessagesViewModel() {

            this.pageCreatedAt = DateTime.Now;
        }

        public ApplicationsMessagesViewModel GenerateViewModel(IEnumerable<Application> systems)
        {
            //ToViewModel(systems);

            return this;
        }
    }


}