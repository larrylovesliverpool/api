﻿using AutoMapper;
using WebApi.Domain;
using WebApi.ViewModels;

namespace WebApi
{
    public class AutoMapperConfig
    {

        public static void RegisterMappers()
        {

            // *****************************************************
            //  Mapper.Initialize(cfg => cfg.CreateMap<from, to>());
            // *****************************************************

            Mapper.Initialize(cfg => { cfg.CreateMap<MessagesCreateViewModel, Message>(); cfg.CreateMap<MessagesAmendViewModel, Message>(); });
            //Mapper.Initialize(cfg => cfg.CreateMap<MessagesAmendViewModel, Message>());
        }

    }
}