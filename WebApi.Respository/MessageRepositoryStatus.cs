﻿using System;
using WebApi.ServiceInterfaces;

namespace WebApi.Respository
{
    public class MessageRepositoryStatus : IStatus
    {
        public MessageRepositoryStatus() {

            this.IsValid = true;
            this.text = String.Empty;
        }

        public bool IsValid { get; set; }

        public string text { get; set; }

        public void CatchMessage()
        {
            throw new NotImplementedException();
        }

    }
}
