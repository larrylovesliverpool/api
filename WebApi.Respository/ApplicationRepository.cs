﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WebApi.Domain;
using WebApi.ServiceInterfaces;
using WebSite.Repository;

namespace WebApi.Respository
{
    public class ApplicationRepository : IApplicationRepository
    {
        private DataAccess connection { get; set; }
        public bool hasData { get; set; }

        public ApplicationRepository() {

            this.connection = new DataAccess("DefaultConnection");
            this.hasData = false;
        }

        public Application Read(int identifier)
        {
            return Read().ToList().Find(p => p.applicationID == identifier); ;
        }

        public IEnumerable<Application> Read()
        {
            List<Application> applications = new List<Application>();

            try
            {
                SqlCommand command = new SqlCommand("Applications_SelectAll", connection.Connection);
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    this.hasData = true;

                    while (reader.Read())
                    {
                        Application application = new Application();

                        application.applicationID = (int)reader["ApplicationsId"];
                        application.name = (string)reader["Name"];
                        application.description = (string)reader["Description"];
                        application.active = (bool)reader["Active"];

                        applications.Add(application);
                    }
                }
                else
                {
                    this.hasData = false;
                }
                reader.Close();

                connection.Close();
            }
            catch ( Exception ex)
            {
                this.hasData = false;
            }

            return applications;
        }

        public IStatus Create(Application T)
        {
            throw new NotImplementedException();
        }

        public IStatus Update(Application T)
        {
            throw new NotImplementedException();
        }

        public IStatus Delete(int identifier)
        {
            throw new NotImplementedException();
        }
    }
}
