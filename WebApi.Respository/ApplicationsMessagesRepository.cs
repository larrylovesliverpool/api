﻿using System;
using System.Collections.Generic;
using WebApi.Domain;
using WebApi.ServiceInterfaces;

namespace WebApi.Respository
{
    public class ApplicationsMessagesRepository : IApplicationsMessagesRepository
    {
        // *********
        // mock data
        // *********
        List<Domain.ApplicationsMessages> applicationsMessages = new List<Domain.ApplicationsMessages>
        {
            new Domain.ApplicationsMessages { text = "Message A1", messageID = 1, startShowing = new DateTime(2017,11,2),numberOfDays = 4, active = true, applicationID = 2, name = "Agency", description = "In house Application."  },
            new Domain.ApplicationsMessages { text = "Message A2", messageID = 2, startShowing = new DateTime(2017,11,22),numberOfDays = 10, active = true, applicationID = 4, name = "Agency", description = "In house Application."  },
            new Domain.ApplicationsMessages { text = "Message A3", messageID = 3, startShowing = new DateTime(2017,11,2),numberOfDays = 4, active = true, applicationID = 8, name = "Agency", description = "In house Application."  },
            new Domain.ApplicationsMessages { text = "Message A4", messageID = 4, startShowing = new DateTime(2017,11,22),numberOfDays = 10, active = true, applicationID = 16, name = "Agency", description = "In house Application." },
            new Domain.ApplicationsMessages { text = "Message A5", messageID = 5, startShowing = new DateTime(2017,11,2),numberOfDays = 4, active = true, applicationID = 32, name = "Agency", description = "In house Application."  },
            new Domain.ApplicationsMessages { text = "Message A6", messageID = 6, startShowing = new DateTime(2017,11,22),numberOfDays = 10, active = true, applicationID = 32, name = "Agency", description = "In house Application."  }
        };

        public IStatus Create(ApplicationsMessages T)
        {
            throw new NotImplementedException();
        }

        public IStatus Delete(int identifier)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Domain.ApplicationsMessages> Read()
        {
            return applicationsMessages;
        }

        public Domain.ApplicationsMessages Read(int identifier)
        {
            return applicationsMessages.Find(p => p.applicationID == identifier);
        }

        public IStatus Update(ApplicationsMessages T)
        {
            throw new NotImplementedException();
        }
    }
}
