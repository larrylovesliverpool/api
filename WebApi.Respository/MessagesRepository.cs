﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebApi.Domain;
using WebApi.ServiceInterfaces;
using WebSite.Repository;

namespace WebApi.Respository
{
    public class MessagesRepository : IMessagesRepository
    {
        private DataAccess connection = new DataAccess("DefaultConnection");
        public bool hasData = false;

        public IStatus Create(Message message)
        {
            IStatus status = new MessageRepositoryStatus();

            try
            {
                SqlCommand command = new SqlCommand("Message_Create", connection.Connection);
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                command.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                command.Parameters.Add("@messageText", SqlDbType.VarChar, 250).Value = message.text;
                command.Parameters.Add("@showDate", SqlDbType.DateTime).Value = message.startShowing;
                command.Parameters.Add("@showFor", SqlDbType.Int).Value = message.numberOfDays;
                command.Parameters.Add("@application", SqlDbType.Int).Value = message.selectedApplication;

                command.ExecuteNonQuery();

                var returnValue = (int)command.Parameters["@RETURN_VALUE"].Value;
                
                if (returnValue != 0)
                {
                    status.IsValid = false;
                    status.text = "Error creating message in Database.";
                }
                else
                {
                    status.IsValid = true;
                    status.text = "Message created in Database.";
                }

                connection.Close();
            }
            catch( Exception ex)
            {
                status.IsValid = false;
                status.text = ex.Message;
            }

            return status;
        }

        public IStatus Amend(Message message)
        {
            IStatus status = new MessageRepositoryStatus();

            try
            {
                SqlCommand command = new SqlCommand("Message_Amend", connection.Connection);
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                command.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                command.Parameters.Add("@messageId", SqlDbType.Int).Value = message.messageID;
                command.Parameters.Add("@messageText", SqlDbType.VarChar, 250).Value = message.text;
                command.Parameters.Add("@showDate", SqlDbType.DateTime).Value = message.startShowing;
                command.Parameters.Add("@showFor", SqlDbType.Int).Value = message.numberOfDays;
                command.Parameters.Add("@application", SqlDbType.Int).Value = message.selectedApplication;

                command.ExecuteNonQuery();

                var returnValue = (int)command.Parameters["@RETURN_VALUE"].Value;

                if (returnValue != 0)
                {
                    status.IsValid = false;
                    status.text = "Error amending message in Database.";
                }
                else
                {
                    status.IsValid = true;
                    status.text = "Message amended in Database.";
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                status.IsValid = false;
                status.text = ex.Message;
            }

            return status;
        }

        public IEnumerable<Message> Read()
        {
            List<Message> messages = new List<Message>();

            try
            {
                SqlCommand command = new SqlCommand("Message_SelectAll", connection.Connection);
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    this.hasData = true;

                    while (reader.Read())
                    {
                        Message message = new Message();

                        message.messageID = (int)reader["MessageId"];
                        message.text = (string)reader["text"];
                        message.startShowing = (DateTime)reader["ShowDate"];
                        message.numberOfDays = (int)reader["ShowFor"];
                        message.selectedApplication = (int)reader["DisplayApplication"];

                        messages.Add(message);
                    }
                }
                else
                {
                    this.hasData = false;
                }
                reader.Close();

                connection.Close();
            }
            catch ( Exception ex)
            {

            }

            return messages;
        }

        public Message Read(int identifier)
        {
            throw new NotImplementedException();
        }

        public IStatus Update(Message message)
        {
            IStatus status = new MessageRepositoryStatus();

            try
            {
                SqlCommand command = new SqlCommand("Message_Amend", connection.Connection);
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                command.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                command.Parameters.Add("@messageId", SqlDbType.Int).Value = message.messageID;
                command.Parameters.Add("@messageText", SqlDbType.VarChar, 250).Value = message.text;
                command.Parameters.Add("@showDate", SqlDbType.DateTime).Value = message.startShowing;
                command.Parameters.Add("@showFor", SqlDbType.Int).Value = message.numberOfDays;
                command.Parameters.Add("@application", SqlDbType.Int).Value = message.selectedApplication;

                command.ExecuteNonQuery();

                var returnValue = (int)command.Parameters["@RETURN_VALUE"].Value;

                if (returnValue != 0)
                {
                    status.IsValid = false;
                    status.text = "Error amending message in Database.";
                }
                else
                {
                    status.IsValid = true;
                    status.text = "Message amended in Database.";
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                status.IsValid = false;
                status.text = ex.Message;
            }

            return status;
        }

        public IStatus Delete(int identifier)
        {
            throw new NotImplementedException();
        }
    }
}

