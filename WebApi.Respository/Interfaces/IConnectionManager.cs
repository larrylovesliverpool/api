﻿using System.Data;
using System.Data.SqlClient;

namespace WebSite.Repository
{
    public interface IConnectionManager
    {
        void Open();

        void Close();

        string connectionString();

        string connectionString(string connectionName);
    }
}
