﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Domain
{
    public class Application : IApplication
    {
        public int applicationID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool active { get; set; }
    }
}
