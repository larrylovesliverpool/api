﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Domain
{
    public interface IMessage
    {
        string text { get; set; }
        int messageID { get; set; }
        DateTime startShowing { get; set; }
        int numberOfDays { get; set; }
    }
}
