﻿using System.Collections.Generic;

namespace WebApi.Domain
{
    public class Messages
    {
        public List<Message> messages { get; set; }
    }
}
