﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Domain
{
    public class ApplicationsMessages : IMessage, IApplication
    {
        public string text { get; set; }
        public int messageID { get; set; }
        public DateTime startShowing { get; set; }
        public int numberOfDays { get; set; }
        public int applicationID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool active { get; set; }

    }
}
