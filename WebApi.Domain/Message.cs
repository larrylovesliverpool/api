﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Domain
{
    public class Message : IMessage
    {
        public string text { get; set; }
        public int messageID { get; set; }
        public DateTime startShowing { get; set; }
        public int numberOfDays { get; set; }
        public int selectedApplication { get; set; }
        public int messageDisplayId { get; set; }
    }
}